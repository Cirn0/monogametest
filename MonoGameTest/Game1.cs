﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.IO;

namespace MonoGameTest
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private IGameScreen _currentScreen;
        private IGameScreen currentScreen
        {
            get
            {
                return _currentScreen;
            }
            set
            {
                var temp = _currentScreen;
                value.Initialize(GraphicsDevice, new Rectangle(Point.Zero, new Point(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight)), Content);
                _currentScreen = value;
                temp?.Dispose();
            }
        }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            Point screenSize = new Point(1366, 768);
            graphics.PreferredBackBufferWidth = screenSize.X;
            graphics.PreferredBackBufferHeight = screenSize.Y;
            graphics.ApplyChanges();

            currentScreen = new MainScreen();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                Exit();
            }

            MouseEvents.Update(gameTime);

            if (currentScreen.Update(gameTime))
            {
                switch (currentScreen)
                {
                    case MainScreen ms:
                        switch (ms.RequiredGameState)
                        {
                            case GameState.Exit:
                                Exit();
                                break;
                            case GameState.LevelSelect:
                                currentScreen = new LevelSelect();
                                break;
                            default:
                                throw new InvalidOperationException();
                        }
                        break;
                    case LevelSelect ls:
                        currentScreen = new Puzzle(ls.RequiredPuzzle);
                        break;
                    default:
                        currentScreen = new MainScreen();
                        break;
                }
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);

            currentScreen.Draw(spriteBatch, gameTime);

            spriteBatch.End();

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }

    public enum GameState
    {
        Exit,
        Menu,
        LevelSelect,
        Puzzle
    }
}
