﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonoGameTest
{
    internal interface IGameScreen : IDisposable
    {
        void Initialize(GraphicsDevice graphicsDevice, Rectangle screen, ContentManager content);
        void Draw(SpriteBatch spriteBatch, GameTime gameTime);
        bool Update(GameTime gameTime);
    }
}
