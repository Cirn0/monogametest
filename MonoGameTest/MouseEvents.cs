﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonoGameTest
{
    internal static class MouseEvents
    {
        public static event EventHandler<MouseEventArgs> MouseMove;
        public static event EventHandler<MouseEventArgs> MouseClick;

        static MouseState prevMouseState;
        public static void Update(GameTime gameTime)
        {
            var currentMouseState = Mouse.GetState();
            if (currentMouseState.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
            {
                OnMouseClick(currentMouseState.Position);
            }

            OnMouseMove(currentMouseState.Position);

            prevMouseState = currentMouseState;
        }

        private static void OnMouseClick(Point position)
        {
            MouseClick?.Invoke(null, new MouseEventArgs(position));
        }

        private static void OnMouseMove(Point position)
        {
            MouseMove?.Invoke(null, new MouseEventArgs(position));
        }
    }

    public class MouseEventArgs : EventArgs
    {
        public MouseEventArgs(Point position)
        {
            MousePosition = position;
        }
        public Point MousePosition { get; set; }
    }
}
