﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MonoGameTest
{
    internal class LevelSelect : IGameScreen
    {
        private class Level
        {
            public Texture2D Thumbnail { get; set; }
            public string FilePath { get; set; }
            public Vector2 DrawingPosition { get; set; }
            public Vector2 DrawingSize { get; set; }
        }

        private Level[] levels;

        public string RequiredPuzzle { get; private set; }

        public void Initialize(GraphicsDevice graphicsDevice, Rectangle screen, ContentManager content)
        {
            levels = new[]
            {
                new Level
                {
                    FilePath = "pics/alice.jpg",
                    Thumbnail = Texture2D.FromFile(graphicsDevice, "pics/alice.jpg")
                },
                new Level
                {
                    FilePath = "pics/flandre.jpg",
                    Thumbnail = Texture2D.FromFile(graphicsDevice, "pics/flandre.jpg")
                },
                new Level
                {
                    FilePath = "pics/haruhi.jpg",
                    Thumbnail = Texture2D.FromFile(graphicsDevice, "pics/haruhi.jpg")
                },
                new Level
                {
                    FilePath = "pics/horo.jpg",
                    Thumbnail = Texture2D.FromFile(graphicsDevice, "pics/horo.jpg")
                },
                new Level
                {
                    FilePath = "pics/madoka.jpg",
                    Thumbnail = Texture2D.FromFile(graphicsDevice, "pics/madoka.jpg")
                }
            };

            int squareSize = (int)Math.Ceiling(Math.Sqrt(levels.Length));

            Vector2 drawingRectLocation = new Vector2(Math.Max((screen.Width - screen.Height) / 2, 0), Math.Max((screen.Height - screen.Width) / 2, 0));
            Vector2 rectSize = new Vector2(Math.Min(screen.Width, screen.Height) / squareSize);

            for (int i = 0; i < levels.Length; ++i)
            {
                int x = i % squareSize;
                int y = i / squareSize;

                float sizeCoeff = (float)levels[i].Thumbnail.Width / levels[i].Thumbnail.Height;
                float multiX = Math.Min(sizeCoeff, 1);
                float multiY = Math.Min(1 / sizeCoeff, 1);
                Vector2 multi = new Vector2(multiX, multiY);

                levels[i].DrawingSize = rectSize * multi;
                levels[i].DrawingPosition = drawingRectLocation + new Vector2(rectSize.X * x, rectSize.Y * y) + ((rectSize - levels[i].DrawingSize) / 2);
            }

            MouseEvents.MouseClick += MouseClick;
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            foreach (var level in levels)
            {
                spriteBatch.Draw(level.Thumbnail, new Rectangle((int)level.DrawingPosition.X, (int)level.DrawingPosition.Y, (int)level.DrawingSize.X, (int)level.DrawingSize.Y), Color.White);
            }
        }

        bool shouldQuit = false;
        public bool Update(GameTime gameTime)
        {
            return shouldQuit;
        }

        private void MouseClick(object sender, MouseEventArgs e)
        {
            var me = GetLevelByPoint(e.MousePosition);
            if (me != default)
            {
                RequiredPuzzle = me.FilePath;
                shouldQuit = true;
            }
        }

        private Level GetLevelByPoint(Point point)
        {
            var temp = levels.Select(a => (a, new Rectangle((int)a.DrawingPosition.X, (int)a.DrawingPosition.Y, (int)a.DrawingSize.X, (int)a.DrawingSize.Y)));
            var elem = temp.FirstOrDefault(a => a.Item2.Contains(point));
            if (elem != default)
            {
                return elem.a;
            }

            return default;
        }

        public void Dispose()
        {
            MouseEvents.MouseClick -= MouseClick;
        }
    }
}
