﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace MonoGameTest
{
    internal class MainScreen : IGameScreen
    {
        private class MenuElement
        {
            public Vector2 Position { get; set; }
            private string text;
            public string Text
            {
                get
                {
                    return text;
                }
                set
                {
                    text = value;
                    measuredText = null;
                }
            }
            public GameState RequiredState { get; set; }
            public SpriteFont Font { get; set; }

            private Vector2? measuredText;
            public Vector2 MeasuredText
            {
                get
                {
                    if (measuredText == null)
                    {
                        measuredText = Font.MeasureString(Text);
                    }
                    return measuredText.Value;
                }
            }
        }

        private MenuElement[] menuElements;
        private MenuElement hoveredElement;

        public GameState RequiredGameState { get; private set; } = (GameState)(-1);

        SpriteFont font;

        public void Initialize(GraphicsDevice graphicsDevice, Rectangle screen, ContentManager content)
        {
            font = content.Load<SpriteFont>("DefaultFont");

            menuElements = new MenuElement[]
            {
                new MenuElement
                {
                    Text = "Play",
                    Font = font,
                    RequiredState = GameState.LevelSelect
                },
                new MenuElement
                {
                    Text = "Exit",
                    Font = font,
                    RequiredState = GameState.Exit
                }
            };

            float maxWidth = menuElements.Max(a => a.MeasuredText.X);
            float measuredX = screen.X + (screen.Width - maxWidth) / 2;

            for (int i = 0; i < menuElements.Length; ++i)
            {
                menuElements[i].Position = new Vector2(measuredX, (((screen.Height / menuElements.Length) - menuElements[i].MeasuredText.Y) / 2) + (screen.Height / menuElements.Length * i));
            }

            MouseEvents.MouseClick += MouseClick;
            MouseEvents.MouseMove += MouseMove;
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            foreach (var elem in menuElements)
            {
                spriteBatch.DrawString(font, elem.Text, elem.Position, elem == hoveredElement ? Color.Red : Color.Black);
            }
        }

        bool shouldQuit = false;
        public bool Update(GameTime gameTime)
        {
            return shouldQuit;
        }

        private void MouseClick(object sender, MouseEventArgs e)
        {
            var me = GetElementByPoint(e.MousePosition);
            if (me != default)
            {
                RequiredGameState = me.RequiredState;
                shouldQuit = true;
            }
        }

        private void MouseMove(object sender, MouseEventArgs e)
        {
            var me = GetElementByPoint(e.MousePosition);
            hoveredElement = me;
        }

        private MenuElement GetElementByPoint(Point point)
        {
            var temp = menuElements.Select(a => (a, new Rectangle((int)a.Position.X, (int)a.Position.Y, (int)a.MeasuredText.X, (int)a.MeasuredText.Y)));
            var elem = temp.FirstOrDefault(a => a.Item2.Contains(point));
            if (elem != default)
            {
                return elem.a;
            }

            return default;
        }

        public void Dispose()
        {
            MouseEvents.MouseClick -= MouseClick;
            MouseEvents.MouseMove -= MouseMove;
        }
    }
}
