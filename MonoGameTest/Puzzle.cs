﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MonoGameTest
{
    internal class Puzzle : IGameScreen
    {
        private readonly int pieceCountX;
        private readonly int pieceCountY;
        private readonly string fileName;
        public Puzzle(string fileName, PuzzleDifficulty difficulty = PuzzleDifficulty.Easy)
        {
            this.fileName = fileName;
            (pieceCountX, pieceCountY) = difficulty switch
            {
                PuzzleDifficulty.Easy => (4, 4),
                PuzzleDifficulty.Medium => (7, 7),
                PuzzleDifficulty.Hard => (9, 9),
                _ => throw new InvalidOperationException()
            };
        }

        Rectangle[,] shuffled;
        Texture2D texture;
        public void Initialize(GraphicsDevice graphicsDevice, Rectangle screen, ContentManager content)
        {
            texture = Texture2D.FromFile(graphicsDevice, fileName);

            shuffled = new Rectangle[pieceCountX, pieceCountY];
            int textureW = texture.Width / pieceCountX;
            int textureH = texture.Height / pieceCountY;
            for (int i = 0; i < pieceCountX; ++i)
            {
                for (int j = 0; j < pieceCountY; ++j)
                {
                    shuffled[i, j] = new Rectangle(textureW * i, textureH * j, textureW, textureH);
                }
            }


            Random rng = new Random();
            for (int i = 0; i < pieceCountX; ++i)
            {
                for (int j = 0; j < pieceCountY; ++j)
                {
                    int tempX = rng.Next(0, pieceCountX);
                    int tempY = rng.Next(0, pieceCountY);
                    var mem = shuffled[i, j];
                    shuffled[i, j] = shuffled[tempX, tempY];
                    shuffled[tempX, tempY] = mem;
                }
            }

            float screenW = screen.Width;
            float screenH = screen.Height;
            bool useW = texture.Width / screenW > texture.Height / screenH;
            scale = useW ? screenW / texture.Width : screenH / texture.Height;
            drawStart = new Vector2(screen.X + (useW ? 0 : (screenW - texture.Width * scale) / 2), screen.Y + (useW ? (screenH - texture.Height * scale) / 2 : 0));

            MouseEvents.MouseClick += MouseClick;
        }

        Vector2 drawStart;
        float scale;

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            //spriteBatch.Draw(texture, new Rectangle(drawStart.ToPoint(), (new Vector2(texture.Width, texture.Height) * scale).ToPoint()), Color.White);

            for (int i = 0; i < pieceCountX; ++i)
            {
                for (int j = 0; j < pieceCountY; ++j)
                {
                    Vector2 newPosition = new Vector2(drawStart.X + texture.Width * scale * i / pieceCountX, drawStart.Y + texture.Height * scale * j / pieceCountY);

                    // Реально отрисовываем немного больше, чем нужно. Связано с неточностями округления при выводе куска текстуры 
                    // с определённым scale на экран. В результате между некоторыми кусочками изображения возникает пустое место 
                    // шириной в 1 пиксель. Простого способа решить проблему не нашёл, поэтому просто отрисовываю на несколько пикселей
                    // из текстуры больше - более правые и нижние кусочки перекроют лишнее, если нужно
                    var rect = new Rectangle(shuffled[i, j].Location, shuffled[i, j].Size);
                    int amountH = i == pieceCountX - 1 ? 0 : 1;
                    int amountV = j == pieceCountY - 1 ? 0 : 1;
                    rect.Inflate(amountH, amountV);
                    rect.Offset(amountH, amountV);

                    spriteBatch.Draw(texture, newPosition, rect, Color.White, 0, Vector2.Zero, new Vector2(scale), SpriteEffects.None, 0);
                }
            }
        }

        bool shouldQuit = false;
        public bool Update(GameTime gameTime)
        {
            return shouldQuit;
        }

        (int i, int j)? selectedPiece;
        private void MouseClick(object sender, MouseEventArgs e)
        {
            Point screenCoord = e.MousePosition;

            Rectangle textRect = new Rectangle((int)drawStart.X, (int)drawStart.Y, (int)(texture.Width * scale), (int)(texture.Height * scale));
            if (!textRect.Contains(screenCoord))
            {
                return;
            }

            Point normalizedScreenCoord = screenCoord - textRect.Location;
            (int i, int j) newPiece = (Math.Min(normalizedScreenCoord.X / (textRect.Width / pieceCountX), pieceCountX - 1), Math.Min(normalizedScreenCoord.Y / (textRect.Height / pieceCountY), pieceCountY - 1));
            if (selectedPiece == null)
            {
                selectedPiece = newPiece;
                return;
            }

            var mem = shuffled[selectedPiece.Value.i, selectedPiece.Value.j];
            shuffled[selectedPiece.Value.i, selectedPiece.Value.j] = shuffled[newPiece.i, newPiece.j];
            shuffled[newPiece.i, newPiece.j] = mem;

            selectedPiece = null;

            if (CheckIsSorted())
            {
                shouldQuit = true;
            }
        }

        bool CheckIsSorted()
        {
            for (int i = 1; i < pieceCountX; ++i)
            {
                for (int j = 1; j < pieceCountY; ++j)
                {
                    if (shuffled[i, j].Left != shuffled[i - 1, j].Right)
                    {
                        return false;
                    }
                    if (shuffled[i,j].Top != shuffled[i, j - 1].Bottom)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public void Dispose()
        {
            MouseEvents.MouseClick -= MouseClick;
        }
    }

    internal enum PuzzleDifficulty
    {
        Easy,
        Medium,
        Hard
    }
}
